# FRQ Test Interface Integration POC

### Following are the components used for demonstrating test interface integration using interface/contracts

* [Consumer One](./src/app/consumer-one)
* [Consumer Two](./src/app/consumer-one)
* [Test Interface](./src/app/test-interface)

#### Contract Definition Provided By Test Interface

```csharp
interface TestInterface{
    getQuestion() : Observable<any>;
    getAnnotation() : Observable<any>;
    saveAnnotation(data : any) : Observable<any>;
    saveResponse(data : any) : Observable<any>;
    saveScore() : Observable<any>;
}
```

Test interface provides input binding of type TestInterface for consumers

#### Integration with consumer one

Consumer One integrates test interface as a component in the following way by providing implementation of TestInterface contract. 

```html

<app-test-interface-wrapper [testInterface]="testInterface">
</app-test-interface-wrapper>

```

Following is the implementation of TestInterface contract by consumer one

```charp
class TestInterfaceOne implements TestInterface{

    public getQuestion() : Observable<any>{
        return new Observable(subscriber => {
            subscriber.next("Question from Consumer One");
            subscriber.complete();
        });
    }
    
    public getAnnotation() : Observable<any>{
        return new Observable(subscriber => {
            subscriber.next("Annotation from Consumer One");
            subscriber.complete();
        });
    }
    
    public saveAnnotation(data : any) : Observable<any>{
        return new Observable(subscriber => {
            subscriber.next("Save annotation from Consumer One");
            subscriber.complete();
        });
    }

    public saveResponse(data : any) : Observable<any>{
        return new Observable(subscriber => {
            subscriber.next("Save Response from Consumer One");
            subscriber.complete();
        });
    }

    
    public saveScore() : Observable<any>{
        return new Observable(subscriber => {
            subscriber.next("Save Score from Consumer One");
            subscriber.complete();
        });
    }
}
```

#### Links

* [FRQ Test Interface Integration Document](https://uworld.atlassian.net/wiki/spaces/~450211530/pages/4007133185/FRQ+Test+Interface+Integration)
* [Breakdown of components in FRQ Test Interface](https://uworld.atlassian.net/wiki/spaces/~450211530/pages/3914039539/FRQ+Test+Interface+-+Breakdown+of+components)