import { Observable } from 'rxjs';
import { TestInterface } from '../test-interface/contracts/test-interface';

/**
 * Implementation of TestInterface contract from consumer one 
 * to communicate with tes interface components.
 */
export class TestInterfaceOne implements TestInterface{

    public getQuestion() : Observable<any>{
        return new Observable(subscriber => {
            subscriber.next("Question from Consumer One");
            subscriber.complete();
        });
    }
    
    public getAnnotation() : Observable<any>{
        return new Observable(subscriber => {
            subscriber.next("Annotation from Consumer One");
            subscriber.complete();
        });
    }
    
    public saveAnnotation(data : any) : Observable<any>{
        return new Observable(subscriber => {
            subscriber.next("Save annotation from Consumer One");
            subscriber.complete();
        });
    }

    public saveResponse(data : any) : Observable<any>{
        return new Observable(subscriber => {
            subscriber.next("Save Response from Consumer One");
            subscriber.complete();
        });
    }

    
    public saveScore() : Observable<any>{
        return new Observable(subscriber => {
            subscriber.next("Save Score from Consumer One");
            subscriber.complete();
        });
    }
}