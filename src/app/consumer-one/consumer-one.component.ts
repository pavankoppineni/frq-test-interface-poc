import { Component, OnInit } from '@angular/core';
import { TestInterface } from '../test-interface/contracts/test-interface';
import { TestInterfaceOne } from './test-interface-one';

@Component({
  selector: 'app-consumer-one',
  templateUrl: './consumer-one.component.html',
  styleUrls: ['./consumer-one.component.css']
})
export class ConsumerOneComponent implements OnInit {

  constructor() { }

  testInterface : TestInterface = new TestInterfaceOne();

  ngOnInit(): void {
  }

}
