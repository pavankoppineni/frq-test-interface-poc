import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsumerOneComponent } from './consumer-one.component';

describe('ConsumerOneComponent', () => {
  let component: ConsumerOneComponent;
  let fixture: ComponentFixture<ConsumerOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsumerOneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumerOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
