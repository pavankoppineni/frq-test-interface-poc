import { Component, OnInit } from '@angular/core';
import { TestInterfaceOne } from '../consumer-one/test-interface-one';
import { TestInterface } from '../test-interface/contracts/test-interface';
import { TestInterfaceTwo } from './test-interface-two';

@Component({
  selector: 'app-consumer-two',
  templateUrl: './consumer-two.component.html',
  styleUrls: ['./consumer-two.component.css']
})
export class ConsumerTwoComponent implements OnInit {

  constructor() { }

  testInterface : TestInterface = new TestInterfaceTwo();

  ngOnInit(): void {
  }
  
}
