import { Observable } from 'rxjs';
import { TestInterface } from '../test-interface/contracts/test-interface';

/**
 * Implementation of TestInterface contract from consumer two 
 * to communicate with tes interface components.
 */
export class TestInterfaceTwo implements TestInterface{

    public getQuestion() : Observable<any>{
        return new Observable(subscriber => {
            subscriber.next("Question from Consumer Two");
            subscriber.complete();
        });
    }
    
    public getAnnotation() : Observable<any>{
        return new Observable(subscriber => {
            subscriber.next("Annotation from Consumer Two");
            subscriber.complete();
        });
    }
    
    public saveAnnotation(data : any) : Observable<any>{
        return new Observable(subscriber => {
            subscriber.next("Save annotation from Consumer Two");
            subscriber.complete();
        });
    }

    public saveResponse(data : any) : Observable<any>{
        return new Observable(subscriber => {
            subscriber.next("Save Response from Consumer Two");
            subscriber.complete();
        });
    }

    public saveScore() : Observable<any>{
        return new Observable(subscriber => {
            subscriber.next("Save Score from Consumer Two");
            subscriber.complete();
        });
    }
}