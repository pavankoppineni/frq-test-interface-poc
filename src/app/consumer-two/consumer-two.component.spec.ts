import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsumerTwoComponent } from './consumer-two.component';

describe('ConsumerTwoComponent', () => {
  let component: ConsumerTwoComponent;
  let fixture: ComponentFixture<ConsumerTwoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsumerTwoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumerTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
