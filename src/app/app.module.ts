import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ScoreComponent } from './test-interface/score/score.component';
import { AssignmentQuestionComponent } from './test-interface/assignment-question/assignment-question.component';
import { FreeResponseComponent } from './test-interface/free-response/free-response.component';
import { TestInterfaceWrapperComponent } from './test-interface/test-interface-wrapper/test-interface-wrapper.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConsumerOneComponent } from './consumer-one/consumer-one.component';
import { ConsumerTwoComponent } from './consumer-two/consumer-two.component';

@NgModule({
  declarations: [
    AppComponent,
    ScoreComponent,
    AssignmentQuestionComponent,
    FreeResponseComponent,
    TestInterfaceWrapperComponent,
    ConsumerOneComponent,
    ConsumerTwoComponent
  ],
  imports: [
    BrowserModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
