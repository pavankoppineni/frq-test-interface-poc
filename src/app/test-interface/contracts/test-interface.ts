import { Observable } from 'rxjs';

/**
 * This is a contract provided for consumers of test interface to 
 * communicate between consumers and test interface.
 */
export interface TestInterface{
    /**
     * 
     */
    getQuestion() : Observable<any>;

    /**
     * 
     */
    getAnnotation() : Observable<any>

    /**
     * 
     * @param data 
     */
    saveAnnotation(data : any) : Observable<any>;

    /**
     * 
     * @param data 
     */
    saveResponse(data : any) : Observable<any>;

    /**
     * 
     */
    saveScore() : Observable<any>;
}