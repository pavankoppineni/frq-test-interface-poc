import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-assignment-question',
  templateUrl: './assignment-question.component.html',
  styleUrls: ['./assignment-question.component.css']
})
export class AssignmentQuestionComponent implements OnInit {


  constructor() { }

  @Output() 
  getQuestionEvent : EventEmitter<string> = new EventEmitter<string>();

  @Output('saveResponse')
  saveResponseEvent : EventEmitter<string> = new EventEmitter<string>();

  ngOnInit(): void {
  }

  getQuestion() : void{
    this.getQuestionEvent.emit('question');
  }

  saveResponse() : void{
    this.saveResponseEvent.emit('response');
  }
}
