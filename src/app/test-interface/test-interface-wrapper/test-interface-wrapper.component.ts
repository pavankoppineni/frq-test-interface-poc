import { Component, Input, OnInit } from '@angular/core';
import { TestInterface } from '../contracts/test-interface';

@Component({
  selector: 'app-test-interface-wrapper',
  templateUrl: './test-interface-wrapper.component.html',
  styleUrls: ['./test-interface-wrapper.component.css']
})
export class TestInterfaceWrapperComponent implements OnInit {

  constructor() { 
  }
  
  @Input()
  testInterface : TestInterface | undefined;

  active : number = 1;

  ngOnInit(): void {
  }

  saveScore() : void{
    this.testInterface?.saveScore()
      .subscribe(response => {
        console.log(response);
    });
  }

  saveResponse() : void{
    this.testInterface?.saveResponse({})
      .subscribe(response => {
        console.log(response);
    });
  }
}
