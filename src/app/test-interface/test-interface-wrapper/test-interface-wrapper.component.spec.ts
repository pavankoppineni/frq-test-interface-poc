import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestInterfaceWrapperComponent } from './test-interface-wrapper.component';

describe('TestInterfaceWrapperComponent', () => {
  let component: TestInterfaceWrapperComponent;
  let fixture: ComponentFixture<TestInterfaceWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestInterfaceWrapperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestInterfaceWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
