import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-free-response',
  templateUrl: './free-response.component.html',
  styleUrls: ['./free-response.component.css']
})
export class FreeResponseComponent implements OnInit {

  constructor() { }

  @Output()
  saveResponseEvent : EventEmitter<string> = new EventEmitter<string>();

  ngOnInit(): void {
  }

  saveResponse() : void {
    this.saveResponseEvent.emit('response');
  }
}
