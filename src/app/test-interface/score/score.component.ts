import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.css']
})
export class ScoreComponent implements OnInit {

  constructor() { }

  @Output('saveScore')
  saveScoreEvent : EventEmitter<string> = new EventEmitter<string>();

  ngOnInit(): void {
  }

  saveScore() : void{
    this.saveScoreEvent.emit('score');
  }
}
